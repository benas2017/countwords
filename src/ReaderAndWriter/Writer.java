package ReaderAndWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Writer {

    public Writer() {
    }

    //metodas, kuris output folderyje perleista map'a issaugo .txt faile:
    private static void writeToTxt(Map<String, Integer> map, String destination) {
        try {
            File newFile = new File(destination);
            FileOutputStream fos = new FileOutputStream(newFile);
            PrintWriter pw = new PrintWriter(fos);

            for (Map.Entry<String, Integer> m : map.entrySet()) {
                pw.println(m.getKey() + "   " + m.getValue());
            }

            pw.flush();
            pw.close();
            fos.close();

        } catch (Exception e) {
            System.out.println("FileNotFoundException or IOException");
        }
    }

    //metodas, kuris zodzius is bendro listo perraso i treemap'a ir apskaiciuoja, kiek kartu jie pasikartojo visuose .txt failuose:
    public Map writeDataFromListToMap(List<String> list) {
        Map<String, Integer> map = new TreeMap<>();
        for (String temp : list) {
            Integer count = map.get(temp);
            map.put(temp, (count == null) ? 1 : count + 1);
        }
        return map;
    }

    //metodas, kuris praskanuoja bendra zodziu ir ju pasikartojimu map'a, patikrina pirmas raides, issaugo i 4 map'us pagal abc,
    // ir iskviecia writeToText(), kuris viska issaugo i .txt nurodytame output folderyje:
    public void writeFromMapToTxt(Map<String, Integer> map, String destinationFolder) {
        Map<String, Integer> wordsAtoG = new TreeMap<>();
        Map<String, Integer> wordsHtoN = new TreeMap<>();
        Map<String, Integer> wordsOtoU = new TreeMap<>();
        Map<String, Integer> wordsVtoZ = new TreeMap<>();

        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getKey().matches("([abcdefg]).*")) {
                wordsAtoG.put(entry.getKey(), entry.getValue());
            } else if (entry.getKey().matches("([hijklmn]).*")) {
                wordsHtoN.put(entry.getKey(), entry.getValue());
            } else if (entry.getKey().matches("([opqrstu]).*")) {
                wordsOtoU.put(entry.getKey(), entry.getValue());
            } else if (entry.getKey().matches("([vwxyz]).*")) {
                wordsVtoZ.put(entry.getKey(), entry.getValue());
            }
        }
        writeToTxt(wordsAtoG, destinationFolder + "\\A-G.txt");
        writeToTxt(wordsHtoN, destinationFolder + "\\H-N.txt");
        writeToTxt(wordsOtoU, destinationFolder + "\\O-U.txt");
        writeToTxt(wordsVtoZ, destinationFolder + "\\V-Z.txt");
    }
}
