import MyCallable.NewCallable;
import ReaderAndWriter.MyJFileChooser;
import ReaderAndWriter.Reader;
import ReaderAndWriter.Writer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class MainApp {
    public static void main(String[] args) {

        List<String> allStrings = new ArrayList<>();
        Reader reader = new Reader();
        Writer writer = new Writer();
        MyJFileChooser pathChooser = new MyJFileChooser();

        //pasirenkame I/O direktorijas ir padaromas input direktorijos .txt failu full path'u listas:
        String input = pathChooser.chooseInput();
        String output = pathChooser.chooseOutput();
        List<String> scannedDirectory = reader.readDirectory(input);

//---------------------------------------------------THREADS (start)----------------------------------------------------
        try {
            //issikvieciam excutorServisa ir .txt failu kiekiu apsibreziam giju skaiciu pool'a:
            ExecutorService exec = Executors.newFixedThreadPool(scannedDirectory.size());
            //listas, kuriame saugosime Future objektus, kuruos grazins prasisukusios gijos:
            List<Future<List<String>>> list = new ArrayList<>();

            for (int i = 0; i < scannedDirectory.size(); i++) {
                //sukuriame Callable task'us, juos paleidziame, grazintus Future objektus prisidedame i lista:

                Callable<List<String>> callable = new NewCallable(scannedDirectory.get(i));
                Future<List<String>> future = exec.submit(callable);
                list.add(future);
            }

            //is Future objektu listo issitraukiame atskirus listus, kuriuose saugomi zodziai is .txt failu, ir juos sudedame i bendra zodziu lista
            //long story short - sudedame visus zodzius i viena bendra lista:
            for (Future<List<String>> fut : list) {
                try {
                    allStrings.addAll(fut.get());
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
            exec.shutdown();

        } catch (IllegalArgumentException e) {
            System.out.println("The folder you selected is empty or has no .txt files inside.");
            System.exit(0);
        }
//---------------------------------------------------THREADS (end)------------------------------------------------------

        //suskaiciuojame, kiek ir kokiu zodziu turime bei patalpiname pagal abc map'e:
        Map<String, Integer> map = writer.writeDataFromListToMap(allStrings);

        //map'o turini irasome i .txt ir isvedame i pasirinkta direktorija
        writer.writeFromMapToTxt(map, output);
    }
}
