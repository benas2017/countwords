package MyCallable;

import ReaderAndWriter.Reader;

import java.util.List;
import java.util.concurrent.Callable;

public class NewCallable implements Callable<List<String>> {

    private Reader reader = new Reader();
    private String fileName;
    private List<String> scannedFile;

    //kiekvienam skanuojamam .txt failui bus sukuriama po gija, kuri ciklo gale grazins to failo zodziu lista

    public NewCallable(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<String> call() {
        this.scannedFile = reader.readFile(fileName);
        return scannedFile;
    }
}
