package ReaderAndWriter;

import javax.swing.*;

public class MyJFileChooser {

    public MyJFileChooser() {
    }

    //folderiu ivesties / isvesties pasirinkimas

    public String chooseInput() {
        JFileChooser inputChooser = new JFileChooser();
        inputChooser.setCurrentDirectory(new java.io.File("."));
        inputChooser.setDialogTitle("Please select folder with .txt files inside");
        inputChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = inputChooser.showOpenDialog(null);
        if (returnVal == JFileChooser.CANCEL_OPTION) {
            System.exit(0);
        }
        return inputChooser.getSelectedFile().getAbsolutePath();
    }


    public String chooseOutput() {
        JFileChooser outputChooser = new JFileChooser();
        outputChooser.setCurrentDirectory(new java.io.File("."));
        outputChooser.setDialogTitle("Please select output folder");
        outputChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = outputChooser.showOpenDialog(null);
        if (returnVal == JFileChooser.CANCEL_OPTION) {
            System.exit(0);
        }
        return outputChooser.getSelectedFile().getAbsolutePath();
    }
}
