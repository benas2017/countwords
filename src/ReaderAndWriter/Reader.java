package ReaderAndWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Reader {

    public Reader() {
    }

    //metodas zodziu skaiciavimui .txt faile:
    private static int countWordsInFile(String file) {
        if (file == null || file.isEmpty()) {
            return 0;
        } else {
            String[] words = file.split("\\s+");
            return words.length;
        }
    }

    //metodas, kuris istrina visus simbolius, kurie nera raides:
    private static String deleteNotLetters(String file) {
        StringBuilder sb = new StringBuilder(file);
        int lengthOfCopyOfFile = file.length();

        for (int i = 0; i < lengthOfCopyOfFile; i++) {
            if (!Character.isAlphabetic(sb.toString().charAt(i)) && !Character.isSpaceChar(sb.toString().charAt(i))) {
                sb.deleteCharAt(i);
                lengthOfCopyOfFile--;
                i--;
            }
        }
        String cleanedText = sb.toString().toLowerCase();
        return cleanedText;
    }

    //metodas, kuris pasirinktame input folderyje praskanuoja failus, atsirenka tik .txt ir ju full path'us issaugo liste:
    public List readDirectory(String directory) {
        List<String> directoryContent = new ArrayList<>();
        File folder = new File(directory);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            File file = listOfFiles[i];
            if (file.isFile() && file.getName().endsWith(".txt")) {
                directoryContent.add(file.getAbsolutePath());
            }
        }
        return directoryContent;
    }

    //metodas, kuri iskviecia atskiros gijos bei kuris skirtas .txt failu zodziu nuskaitymui ir ju issaugojimui liste:
    public List readFile(String file) {
        List<String> newList = new ArrayList<>();
        String text = "";

        try {
            Scanner s = new Scanner(new File(file));
            while (s.hasNext()) {
                text = text + s.next() + " ";
            }

            String cleanedText = deleteNotLetters(text);
            int wordCount = countWordsInFile(cleanedText);
            Scanner s2 = new Scanner(cleanedText);

            while (s2.hasNext()) {
                for (int i = 0; i < wordCount; i++) {
                    newList.add(i, s2.next());
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
        return newList;
    }
}
